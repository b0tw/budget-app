const express = require('express');
const router = express.Router();
const other = require('../controllers').Other;

router.get('/reset', other.reset);

module.exports = router;