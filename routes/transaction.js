const express = require('express');
const router = express.Router();
const transaction = require('../controllers').Transaction;

router.get('/displayAll', transaction.displayAll);
router.post('/displayAllByCategory', transaction.displayAllByCategory);

router.post('/displayByDay', transaction.displayByDay);
router.post('/displayByDayByCategory', transaction.displayByYearByCategory);

router.post('/displayByMonth', transaction.displayByMonth);
router.post('/displayByMonthByCategory', transaction.displayByMonthByCategory);

router.post('/displayByYear', transaction.displayByYear);
router.post('/displayByYearByCategory', transaction.displayByYearByCategory);

router.post('/displaySum', transaction.displaySum);
router.post('/displaySumByCategory', transaction.displaySumByCategory);

router.post('/displaySumDay', transaction.displaySumDay);
router.post('/displaySumDayByCategory', transaction.displaySumDayByCategory);

router.post('/displaySumMonth', transaction.displaySumMonth);
router.post('/displaySumMonthByCategory', transaction.displaySumMonthByCategory);

router.post('/displaySumYear', transaction.displaySumYear);
router.post('/displaySumYearByCategory', transaction.displaySumYearByCategory);

router.post('/add', transaction.add);

module.exports = router;