const express = require('express');
const router = express.Router();
const transactionRouter = require('./transaction');
const otherRouter = require('./other');

router.use('/transaction', transactionRouter);
router.use('/other', otherRouter);

module.exports = router;