function generateTable (data) {
    // id, name, price, category, date
    let table = document.createElement('table')
    let tableRow = document.createElement('tr');
    let tableHead = document.createElement('thead');
    let properties;
    let tableBody = document.createElement('tbody');

    data.forEach(element => {
        properties = Object.keys(element);
        let values = Object.values(element);
        let tRow = document.createElement('tr');
        for (let i = 0; i < values.length; i++) {
            let tableData = document.createElement('td');
            let tableText = document.createTextNode(values[i]);
            tableData.appendChild(tableText);
            tRow.appendChild(tableData);
        }
        tableBody.appendChild(tRow);
    });

    properties.forEach((item) => {
        let tableH = document.createElement('th');
        let tableText = document.createTextNode(item);
        tableH.appendChild(tableText);
        tableRow.appendChild(tableH);
    })
    tableHead.appendChild(tableRow);


    table.appendChild(tableHead);
    table.appendChild(tableBody);
    let divTable = document.querySelector('#table');
    divTable.appendChild(table);
}

document.querySelector('#add').addEventListener('click', () => {
    const name = document.querySelector('#name').value.trim();
    const price = document.querySelector('#price').value.trim();
    const category = document.querySelector('#categoryAdd').value;
    const date = document.querySelector('#date').value;

    let transaction = {
        name,
        price,
        category,
        date
    }
    axios.post('/api/transaction/add', transaction).then(response => {
        console.log(response);
    }).catch(error => {
        console.log(error);
    });
})

document.querySelector('#view').addEventListener('click', () => {
    const year = document.querySelector('#year').value.trim();
    const month = document.querySelector('#month').value.trim();
    const day = document.querySelector('#day').value.trim();
    const category = document.querySelector('#categoryView').value;
    const type = document.querySelector('#type').value;
    if (category) {
        if (type == "displayTransactions") {
            if (day) {
                //post ByDay
                //type doesn't exist
                let display = {
                    year,
                    month,
                    day,
                    category
                }
                axios.post('/api/transaction/displayByDayByCategory', display).then(response => {
                    generateTable(response.data);
                });;
            }
            else if (month) {
                //post ByMonth
                //type can exist
                let display = {
                    year,
                    month,
                    category
                }
                axios.post('/api/transaction/displayByMonthByCategory', display).then(response => {
                    generateTable(response.data);
                });;
            }
            else if (year) {
                //post ByYear
                //type can exist
                let display = {
                    year,
                    category
                }
                axios.post('/api/transaction/displayByYearByCategory', display).then(response => {
                    generateTable(response.data);
                });;
            }
            else {
                let display = {
                    category
                }
                axios.post('/api/transaction/displayAllByCategory', display).then(response => {
                    generateTable(response.data);
                });;
                //post all
            }
        }
        else {
            if (day) {
                //post ByDay
                //type doesn't exist
                let display = {
                    year,
                    month,
                    day,
                    category,
                    type
                }
                axios.post('/api/transaction/displaySumDayByCategory', display);
            }
            else if (month) {
                //post ByMonth
                //type can exist
                let display = {
                    year,
                    month,
                    category,
                    type
                }
                axios.post('/api/transaction/displaySumMonthByCategory', display);
            }
            else if (year) {
                //post ByYear
                //type can exist
                let display = {
                    year,
                    category,
                    type
                }
                axios.post('/api/transaction/displaySumYearByCategory', display);
            }
            else {
                let display = {
                    category,
                    type
                }
                axios.post('/api/transaction/displaySumByCategory', display);
                //post all
            }
        }
    }
    else {
        if (type == "displayTransactions") {
            if (day) {
                //post ByDay
                //type doesn't exist
                let display = {
                    year,
                    month,
                    day
                }
                axios.post('/api/transaction/displayByDay', display).then(response => {
                    generateTable(response.data);
                });;
            }
            else if (month) {
                //post ByMonth
                //type can exist
                let display = {
                    year,
                    month
                }
                axios.post('/api/transaction/displayByMonth', display).then(response => {
                    generateTable(response.data);
                });;
            }
            else if (year) {
                //post ByYear
                //type can exist
                let display = {
                    year
                }
                axios.post('/api/transaction/displayByYear', display).then(response => {
                    generateTable(response.data);
                });;
            }
            else {
                axios.get('/api/transaction/displayAll').then(response => {
                    generateTable(response.data);
                });
                //post all
            }
        }
        else {
            if (day) {
                //post ByDay
                //type doesn't exist
                let display = {
                    year,
                    month,
                    day,
                    type
                }
                axios.post('/api/transaction/displaySumDay', display);
            }
            else if (month) {
                //post ByMonth
                //type can exist
                let display = {
                    year,
                    month,
                    type
                }
                axios.post('/api/transaction/displaySumMonth', display);
            }
            else if (year) {
                //post ByYear
                //type can exist
                let display = {
                    year,
                    type
                }
                axios.post('/api/transaction/displaySumYear', display);
            }
            else {
                let display = {
                    type
                }
                axios.post('/api/transaction/displaySum', display);
                //post all
            }
        }
    }
})