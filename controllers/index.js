const Transaction = require('./transaction');
const Other = require('./other');

const controller = {
    Transaction,
    Other
};

module.exports = controller;
