const TransactionDB = require('../models').Transanction;
const sequelize = require('sequelize');

function leapYear (year) {
    if (year % 4 != 0)
        return false;
    else if (year % 100 != 0)
        return true;
    else if (year % 400 != 0)
        return false;
    else return true;
}

const controller = {
    displayAll: async (req, res) => {
        TransactionDB.findAll({ order: [['date', 'DESC']] }).then(transactions => {
            res.status(200).send(transactions);
        }).catch(() => {
            res.status(500).send({ message: "Something bad happened." });
        })
    },
    displayAllByCategory: async (req, res) => {
        const category = req.body.category;
        TransactionDB.findAll({ where: { category: category } }, { order: [['date', 'DESC']] }).then(transactions => {
            res.status(200).send(transactions);
        }).catch(() => {
            res.status(500).send({ message: "Something bad happened." });
        })
    },
    displayByDay: async (req, res) => {
        const day = req.body.day;
        const month = req.body.month;
        const year = req.body.year;
        const op = sequelize.Op;

        TransactionDB.findAll({
            where: {
                [op.and]: [
                    sequelize.where(sequelize.fn('DAY', sequelize.col('date')), day),
                    sequelize.where(sequelize.fn('MONTH', sequelize.col('date')), month),
                    sequelize.where(sequelize.fn('YEAR', sequelize.col('date')), year)

                ]
            },
            order: [['date', 'DESC']]
        })
            .then(transaction => {
                res.status(200).send(transaction);
            })
            .catch(() => {
                res.status(500).send({ message: "Something bag happened." });
            })
    },
    displayByDayByCateogry: async (req, res) => {
        const category = req.body.category;
        const year = req.body.year;
        const month = req.body.month;
        const day = req.body.day;
        const op = sequelize.Op;

        TransactionDB.findAll({
            where: {
                [op.and]: [
                    sequelize.where(sequelize.fn('YEAR', sequelize.col('date')), year),
                    sequelize.where(sequelize.fn('MONTH', sequelize.col('date')), month),
                    sequelize.where(sequelize.fn('DAY', sequelize.col('date')), day),
                    { category: category }
                ]
            },
            order: [['date', 'DESC']]
        }).then(transaction => {
            res.status(200).send(transaction);
        }).catch(() => {
            res.status(500).send({ message: "Something" })
        })
    },
    displayByMonth: async (req, res) => {
        const month = req.body.month;
        const year = req.body.year;
        const op = sequelize.Op;

        TransactionDB.findAll({
            where: {
                [op.and]: [
                    sequelize.where(sequelize.fn('MONTH', sequelize.col('date')), month),
                    sequelize.where(sequelize.fn('YEAR', sequelize.col('date')), year)
                ]
            }, order: [['date', 'DESC']]
        })
            .then(transaction => {
                res.status(200).send(transaction);
            })
            .catch(() => {
                res.status(500).send({ message: "Something bag happened." });
            })
    },
    displayByMonthByCategory: async (req, res) => {
        const category = req.body.category;
        const year = req.body.year;
        const month = req.body.month;
        const op = sequelize.Op;

        TransactionDB.findAll({
            where: {
                [op.and]: [
                    sequelize.where(sequelize.fn('YEAR', sequelize.col('date')), year),
                    sequelize.where(sequelize.fn('MONTH', sequelize.col('date')), month),
                    { category: category }
                ]
            },
            order: [['date', 'DESC']]
        }).then(transaction => {
            res.status(200).send(transaction);
        }).catch(() => {
            res.status(500).send({ message: "Something" })
        })
    },
    displayByYear: async (req, res) => {
        const year = req.body.year;

        TransactionDB.findAll(
            {
                where: sequelize.where(sequelize.fn('YEAR', sequelize.col('date')), year),
                order: [['date', 'DESC']]
            }).then(transaction => {
                res.status(200).send(transaction);
            }).catch(() => {
                res.status(500).send({ message: "Something bad happened" });
            })
    },
    displayByYearByCategory: async (req, res) => {
        const category = req.body.category;
        const year = req.body.year;
        const op = sequelize.Op;

        TransactionDB.findAll({
            where: {
                [op.and]: [
                    sequelize.where(sequelize.fn('YEAR', sequelize.col('date')), year),
                    { category: category }
                ]
            },
            order: [['date', 'DESC']]
        }).then(transaction => {
            res.status(200).send(transaction);
        }).catch(() => {
            res.status(500).send({ message: "Something" })
        })
    },
    displaySum: async (req, res) => {
        TransactionDB.sum('price').then(sum => {
            res.status(200).send({ sum });
        }).catch(() => {
            res.status(500).send({ message: "Something bad happened" })
        })
    },
    displaySumByCategory: async (req, res) => {
        const category = req.body.category;

        TransactionDB.sum('price', { where: { category: category } }).then(sum => {
            res.status(200).send({ sum });
        }).catch(() => {
            res.status(500).send({ message: "Something bad happened" })
        })
    },
    displaySumDay: async (req, res) => {
        const day = req.body.day;
        const month = req.body.month;
        const year = req.body.year;
        const op = sequelize.Op;

        TransactionDB.sum('price', {
            where: {
                [op.and]: [
                    sequelize.where(sequelize.fn('DAY', sequelize.col('date')), day),
                    sequelize.where(sequelize.fn('MONTH', sequelize.col('date')), month),
                    sequelize.where(sequelize.fn('YEAR', sequelize.col('date')), year)
                ]
            },
            order: [['date', 'DESC']]
        }).then(sum => {
            res.status(200).send({ sum });
        }).catch(() => {
            res.status(500).send({ message: "Something bad happened" })
        })
    },
    displaySumDayByCategory: async (req, res) => {
        const year = req.body.year;
        const month = req.body.month;
        const day = req.body.day;
        const category = req.body.category;
        const op = sequelize.Op;

        TransactionDB.sum('price', {
            where: {
                [op.and]: [
                    sequelize.where(sequelize.fn('YEAR', sequelize.col('date')), year),
                    sequelize.where(sequelize.fn('MONTH', sequelize.col('date')), month),
                    sequelize.where(sequelize.fn('DAY', sequelize.col('date')), day),
                    { category: category }
                ]
            }
        }).then(sum => {
            res.status(200).send({ sum });
        }).catch(() => {
            res.status(500).send({ message: "Something bad happened" });
        })
    },
    displaySumMonth: async (req, res) => {
        const month = req.body.month;
        const year = req.body.year;
        const op = sequelize.Op;
        const type = req.body.type;
        //need to add req.body validation

        TransactionDB.sum('price', {
            where: {
                [op.and]: [
                    sequelize.where(sequelize.fn('MONTH', sequelize.col('date')), month),
                    sequelize.where(sequelize.fn('YEAR', sequelize.col('date')), year)
                ]
            }
        }).then(sum => {
            let data;
            if (type == 'sum') {
                data = sum;
            }
            else if (type == 'avgDay') {
                var noDays;
                if (leapYear(year) && month == 2) {
                    noDays = 29;
                }
                else if (month == 2) {
                    noDays = 28;
                }
                else if (month == 4 || month == 6 || month == 9 || month == 11) {
                    noDays = 30;
                }
                else {
                    noDays = 31;
                }
                data = sum / noDays;
            }
            res.status(200).send({ data });
        }).catch(() => {
            res.status(500).send({ message: "Something bad happened" })
        })
    },
    displaySumMonthByCategory: async (req, res) => {
        const category = req.body.category;
        const month = req.body.month;
        const year = req.body.year;
        const op = sequelize.Op;
        const type = req.body.type;
        //need to add data req.body validation

        TransactionDB.sum('price', {
            where: {
                [op.and]: [
                    sequelize.where(sequelize.fn('MONTH', sequelize.col('date')), month),
                    sequelize.where(sequelize.fn('YEAR', sequelize.col('date')), year),
                    { category: category }
                ]
            }
        }).then(sum => {
            let data;
            if (type == 'sum') {
                data = sum;
            }
            else if (type == 'avgDay') {
                var noDays;
                if (leapYear(year) && month == 2) {
                    noDays = 29;
                }
                else if (month == 2) {
                    noDays = 28;
                }
                else if (month == 4 || month == 6 || month == 9 || month == 11) {
                    noDays = 30;
                }
                else {
                    noDays = 31;
                }
                data = sum / noDays;
            }
            res.status(200).send({ data });
        }).catch(() => {
            res.status(500).send({ message: "Something bad happened" })
        })
    },
    displaySumYear: async (req, res) => {
        const year = req.body.year;
        const type = req.body.type;
        //need to add data validation
        // if (type != 'sum' || type != 'avgMonth' || type != 'avgDay')
        //     res.status(500).send({ message: 'unknown type' });

        TransactionDB.sum('price', {
            where: sequelize.where(sequelize.fn('YEAR', sequelize.col('date')), year)
        }).then(sum => {
            let data;
            if (type == 'sum') {
                data = sum;
            }
            else if (type == 'avgMonth') {
                data = sum / 12;
            }
            else if (type == 'avgDay') {
                if (leapYear(year)) {
                    noDays = 366;
                }
                else noDays = 365;
                data = sum / noDays;
            }
            res.status(200).send({ data });
        }).catch(() => {
            res.status(500).send({ message: "Something bad happened" });
        })
    },
    displaySumYearByCategory: async (req, res) => {
        const year = req.body.year;
        const category = req.body.category;
        const op = sequelize.Op;
        const type = req.body.type;
        // need to add data validation
        // if (type != 'sum' || type != 'avgMonth' || type != 'avgYear')
        //     res.status(500).send({ message: 'unknown type' });

        TransactionDB.sum('price', {
            where: {
                [op.and]: [
                    sequelize.where(sequelize.fn('YEAR', sequelize.col('date')), year),
                    { category: category }
                ]
            }
        }).then(
            sum => {
                let data;
                if (type == 'sum') {
                    data = sum;
                }
                else if (type == 'avgMonth') {
                    data = sum / 12;
                }
                else if (type == 'avgYear') {
                    if (leapYear(year)) {
                        noDays = 366;
                    }
                    else noDays = 365;
                    data = sum / noDays;
                }
                res.status(200).send({ data });

            }).catch(() => {
                res.status(500).send({ message: "Something bad happened" });
            })
    },
    add: async (req, res) => {

        const transaction = {
            name: req.body.name,
            price: req.body.price,
            date: req.body.date,
            category: req.body.category
        }
        TransactionDB.create(transaction).then(() => {
            res.status(200).send({ message: "Transaction saved." });
        }).catch(() => {
            res.status(500).send({ message: "Something bad happened." });
        })
    }
}

module.exports = controller;