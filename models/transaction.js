module.exports = (sequelize, DataTypes) => {
    return sequelize.define("transaction", {
        name: {
            type: DataTypes.STRING
        },
        price: {
            type: DataTypes.FLOAT
        },
        category: {
            type: DataTypes.STRING
        },
        date: {
            type: DataTypes.DATE
        }
    }, { updatedAt: false })
}