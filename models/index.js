const db = require('../config').db;

const Transanction = db.import('./transaction');

module.exports = {
    Transanction,
    connection: db
};
